﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment3oop
{
    abstract class Animal
    {
        public abstract void animalSound();
        public void sleep()
        {
            Console.WriteLine("Zzz");
        }
    }

    class Pig : Animal
    {
        public override void animalSound()
        {
            Console.WriteLine("The pig says: wee wee");
        }
    }
    class Dog : Animal
    {
        public override void animalSound()
        {
            Console.WriteLine("The dog says: bow wow");
        }
    }
    interface Ibird
    {
        void BirdSound();
    }


    class Pigion : Ibird
    {
        public void BirdSound()
        {

            Console.WriteLine("The Pigion says: chu chu");
        }
    }

    internal class Program
    {
        public static int a = 10;
        static void MyMethod()
        {
            Console.WriteLine(a);
        }

        static void Main(string[] args)
        {
            MyMethod();
            Pig newobject = new Pig();
            newobject.animalSound();
            newobject.sleep();
            Dog dogobject = new Dog();
            dogobject.animalSound();
            Pigion Pobject = new Pigion();
            Pobject.BirdSound();
            Cuboid c = new Cuboid();
            c.length = 4.5;
            c.breadth = 3.5;
            c.height = 5;
            c.Display();
            Console.ReadLine();

        }
    }
    class Cuboid
    {

        public double length;
        public double breadth;
        public double height;

        public double GetArea()
        {
            return 2 * height * (length + breadth);
        }
        public void Display()
        {
            Console.WriteLine("Length: {0}", length);
            Console.WriteLine("breadth: {0}", breadth);
            Console.WriteLine("height: {0}", height);
            Console.WriteLine("Area: {0}", GetArea());
        }
    }
}
